import { expect } from 'chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Tasks, getTasks, createTask } from '/imports/api/tasks';

beforeEach(() => {
  resetDatabase();
});

describe('tasks.get', function() {
  beforeEach(() => {
    const testData = [
      {
        title: 'Task1',
        status: 'incomplete'
      },
      {
        title: 'Task2',
        status: 'completed'
      },
      {
        title: 'Task3',
        status: 'completed',
        isPrivate: true
      }
    ];
    testData.forEach(task => Tasks.insert(task));
  });

  it('returns a list', () => {
    const results = getTasks.call();
    expect(results)
      .to.be.an('array')
      .that.has.lengthOf(2);
  });

  it('returns only non private tasks', () => {
    const results = getTasks.call();
    const titles = results.map(task => task.title);

    expect(titles).to.deep.eq(['Task1', 'Task2']);
  });
});

describe('tasks.create', function() {
  it('creates a task with valid fields', () => {
    const task = {
      title: 'Task 1',
      status: 'incomplete'
    };
    createTask.call(task);

    const storedTasks = Tasks.find().fetch();

    expect(storedTasks).to.have.lengthOf(1);
    expect(storedTasks[0]).to.contain(task);
  });

  describe('invalid fields', () => {
    [
      {
        task: { title: '' },
        expectedError: 'Title must be at least 1 characters',
        name: 'Empty title'
      },
      {
        task: { status: 'invalid' },
        expectedError: 'invalid is not an allowed value',
        name: 'Invalid status'
      },
      {
        task: { isPrivate: true },
        expectedError: 'isPrivate is not allowed by the schema',
        name: 'Not allowed field'
      }
    ].forEach(testcase => {
      it(`does not create a task (${testcase.name})`, () => {
        expect(() => createTask.call(testcase.task)).to.throw(
          testcase.expectedError
        );

        expect(Tasks.find().fetch()).to.have.lengthOf(0);
      });
    });
  });
});
