import groupBy from 'lodash/fp/groupBy';
import capitalize from 'lodash/fp/capitalize';
import React, { useMemo } from 'react';
import Task from './task';

const groupTasksByStatus = groupBy(task => capitalize(task.status));

const TasksList = ({ tasks }) => {
  const groupedTasks = useMemo(() => groupTasksByStatus(tasks), [tasks]);
  return (
    <div className="groups-wrapper">
      {Object.entries(groupedTasks).map(([status, values]) => (
        <div className="group-container open" key={status}>
          <div className="group-head">
            <p>{status}</p>
          </div>
          <div className="group-content">
            {values.map(task => (
              <Task task={task} key={task._id} />
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default TasksList;
