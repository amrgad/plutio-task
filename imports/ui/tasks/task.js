import React from 'react';

const Task = ({ task }) => {
  const hasDescription = task.hasDescription === true;
  const hasComments = task.commentsCount > 0;
  const hasFiles = task.filesCount > 0;
  const hasTimer = task.totalTimeTracked > 0;

  const hasIndicators = hasDescription || hasComments || hasFiles || hasTimer;
  return (
    <div className="task">
      <a
        className={`task-link ${task.color ? 'has-color-tag' : ''}`}
        href="#"
        style={{ borderColor: task.color ?? 'transparent' }}
      >
        <div className="task-link-head">
          <div className="task-checkbox">
            <label className="checkbox">
              <input
                type="checkbox"
                defaultChecked={task.status === 'completed'}
              />
              <span className="checkbox-toggle">
                <i className="checkbox-icon icon-check" />
              </span>
            </label>
          </div>
          <div className="task-title">
            <p>{task.title}</p>
          </div>
        </div>
        <div className="task-link-body">
          {hasIndicators && (
            <div className="indicators">
              {hasDescription && (
                <span className="icon-indicator">
                  <i className="icon-description" />
                </span>
              )}
              {hasComments && (
                <span className="icon-indicator">
                  <i className="icon-chat" />
                </span>
              )}
              {hasFiles && (
                <span className="icon-indicator">
                  <i className="icon-attach" />
                </span>
              )}
              {hasTimer && (
                <span className="icon-indicator">
                  <i className="icon-timer" />
                </span>
              )}
            </div>
          )}
          {task.customFields?.length > 0 && (
            <div className="custom-fields">
              {task.customFields.map(field => (
                <div
                  className="custom-field custom-field-single"
                  key={field._id}
                >
                  <p className="color-light has-color-tag">
                    <span className="field-background" />
                    <span className="field-title">{field.value}</span>
                  </p>
                </div>
              ))}
            </div>
          )}
        </div>
      </a>
    </div>
  );
};

export default Task;
