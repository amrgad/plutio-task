import React from 'react';

const AddTask = ({ onSubmit }) => {
  return (
    <form className="add-task" noValidate="" onSubmit={onSubmit}>
      <div>
        <div className="fieldset add-task-input fieldset-stripped">
          <div className="fieldset-content">
            <label className="fieldset-label">
              <span className="fieldset-label-content has-icon">
                <i className="icon-plus" />
              </span>
              <input
                name="title"
                placeholder="Add new task"
                type="text"
                autoComplete="off"
              />
            </label>
          </div>
        </div>
      </div>
    </form>
  );
};

export default AddTask;
