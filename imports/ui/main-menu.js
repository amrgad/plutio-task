import React from 'react';

const MenuItem = ({ title, icon, active, ...props }) => {
  return (
    <a className={`list-item ${active ? 'active' : ''}`} {...props}>
      <i className={`item-icon icon-${icon}`} />
      <p>{title}</p>
    </a>
  );
};

const MainMenu = () => {
  return (
    <div className="app-mainmenu">
      <div className="container">
        <div className="menu-list">
          <MenuItem title="Tasks" icon="check" active href="#" />
          <MenuItem title="Projects" icon="toolkit" href="#" />
          <MenuItem title="People" icon="user" href="#" />
          <MenuItem title="Calendar" icon="calendar" href="#" />
          <MenuItem title="Inbox" icon="inbox" href="#" />
          <MenuItem title="Timesheets" icon="timer" href="#" />

          <hr />
          <MenuItem title="Settings" icon="settings" href="#" />
          <MenuItem
            title="Support"
            icon="question-mark"
            onClick={() => console.log('support')}
          />
        </div>
      </div>
    </div>
  );
};

export default MainMenu;
