import React, { useEffect, useState } from 'react';
import { getTasks, createTask } from '../api/tasks';
import MainMenu from './main-menu';
import PageHeader from './page-header';
import AddTask from './tasks/add-task';
import TasksList from './tasks/tasks-list';

const App = () => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    fetchTasks();
  }, []);

  const fetchTasks = () => {
    getTasks.call((err, results) => {
      setTasks(results);
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    e.persist();

    const title = e.target.elements.title.value;

    const task = {
      title,
      status: 'incomplete'
    };

    try {
      createTask.call(task);

      e.target.reset();
      fetchTasks();
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <>
      <MainMenu />
      <div className="page">
        <PageHeader title="Tasks" />
        <div className="main-tasks-page content list-view">
          <AddTask onSubmit={handleSubmit} />

          <TasksList tasks={tasks} />
        </div>
      </div>
    </>
  );
};

export default App;
