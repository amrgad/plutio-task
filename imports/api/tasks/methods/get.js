import { Meteor } from 'meteor/meteor';
import { Tasks } from '../model/schema';

export const getTasks = {
  name: 'tasks.getTasks',

  run() {
    return Tasks.find({
      isPrivate: {
        $ne: true
      }
    }).fetch();
  },

  call(callback) {
    return Meteor.apply(this.name, [], callback);
  }
};

Meteor.methods({
  [getTasks.name]: function(args) {
    return getTasks.run.call(this, args);
  }
});
