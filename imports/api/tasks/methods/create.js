import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Tasks } from '../model/schema';

export const createTask = new ValidatedMethod({
  name: 'tasks.createTask',
  validate: Tasks.schema
    .pick('title', 'dueDate', 'repeat', 'status', 'customFields', 'color')
    .validator(),
  run(task) {
    return Tasks.insert(task);
  }
});
