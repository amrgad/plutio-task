import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { render } from 'react-dom';
import App from '/imports/ui/app';

Tracker.autorun(() => {
  Meteor.startup(() => {
    render(<App />, document.getElementById('app'));
  });
});
